Package: ParamHelpers
Title: Helpers for Parameters in Black-Box Optimization, Tuning and
        Machine Learning
Description: Functions for parameter descriptions and operations in black-box
    optimization, tuning and machine learning. Parameters can be described
    (type, constraints, defaults, etc.), combined to parameter sets and can in
    general be programmed on. A useful OptPath object (archive) to log function
    evaluations is also provided.
Authors@R: c(
    person("Bernd", "Bischl", email = "bernd_bischl@gmx.net", role = "aut"),
    person("Michel", "Lang", email = "michellang@gmail.com", role = "aut"),
    person("Jakob", "Richter", email = "code@jakob-r.de", role = c("aut", "cre")),
    person("Jakob", "Bossek", email = "j.bossek@gmail.com", role = "aut"),
    person("Daniel", "Horn", email = "daniel.horn@tu-dortmund.de", role = "aut"),
    person("Karin", "Schork", email = "karin.schork@tu-dortmund.de", role = "ctb"),
    person("Pascal", "Kerschke", email = "kerschke@uni-muenster.de", role = "aut"))
URL: https://github.com/berndbischl/ParamHelpers
BugReports: https://github.com/berndbischl/ParamHelpers/issues
License: BSD_2_clause + file LICENSE
Imports: backports, BBmisc (>= 1.10), checkmate (>= 1.8.2), fastmatch,
        methods
Suggests: akima, eaf, emoa, GGally, ggplot2, gridExtra, grid, irace (>=
        2.1), lhs, plyr, reshape2, testthat
LazyData: yes
ByteCompile: yes
Version: 1.12
RoxygenNote: 6.1.1
NeedsCompilation: yes
Packaged: 2019-01-18 14:27:42 UTC; richter
Author: Bernd Bischl [aut],
  Michel Lang [aut],
  Jakob Richter [aut, cre],
  Jakob Bossek [aut],
  Daniel Horn [aut],
  Karin Schork [ctb],
  Pascal Kerschke [aut]
Maintainer: Jakob Richter <code@jakob-r.de>
Repository: CRAN
Date/Publication: 2019-01-18 15:10:11 UTC
Built: R 3.5.3; x86_64-w64-mingw32; 2019-03-24 02:09:03 UTC; windows
Archs: i386, x64
