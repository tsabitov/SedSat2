<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Probability Calibration Plot</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for calibration {caret}"><tr><td>calibration {caret}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Probability Calibration Plot</h2>

<h3>Description</h3>

<p>For classification models, this function creates a 'calibration plot' that describes 
how consistent model probabilities are with observed event rates.
</p>


<h3>Usage</h3>

<pre>
calibration(x, ...)

## Default S3 method:
calibration(x, ...)

## S3 method for class 'formula'
calibration(x, data = NULL, class = NULL,
  cuts = 11, subset = TRUE, lattice.options = NULL, ...)

## S3 method for class 'calibration'
print(x, ...)

## S3 method for class 'calibration'
xyplot(x, data = NULL, ...)

## S3 method for class 'calibration'
ggplot(data, ..., bwidth = 2, dwidth = 3)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>
<p>a <code>lattice</code> formula (see <code><a href="../../lattice/html/xyplot.html">xyplot</a></code> for syntax) where the left
-hand side of the formula is a factor class variable of the observed outcome and the right-hand side 
specifies one or model columns corresponding to a numeric ranking variable for a model (e.g. class 
probabilities). The classification variable should have two levels.</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>
<p>options to pass through to <code><a href="../../lattice/html/xyplot.html">xyplot</a></code> or the panel function (not 
used in <code>calibration.formula</code>).</p>
</td></tr>
<tr valign="top"><td><code>data</code></td>
<td>
<p>For <code>calibration.formula</code>, a data frame (or more precisely, anything that is a valid 
<code>envir</code> argument in <code>eval</code>, e.g., a list or an environment) containing values for any 
variables in the formula, as well as <code>groups</code> and <code>subset</code> if applicable. If not found in 
<code>data</code>, or if <code>data</code> is unspecified, the variables are looked for in the environment of the 
formula. This argument is not used for <code>xyplot.calibration</code>. For ggplot.calibration, <code>data</code> 
should be an object of class &quot;<code>calibration</code>&quot;.&quot;</p>
</td></tr>
<tr valign="top"><td><code>class</code></td>
<td>
<p>a character string for the class of interest</p>
</td></tr>
<tr valign="top"><td><code>cuts</code></td>
<td>
<p>If a single number this indicates the number of splits of the data are used to create the 
plot. By default, it uses as many cuts as there are rows in <code>data</code>. If a vector, these are the 
actual cuts that will be used.</p>
</td></tr>
<tr valign="top"><td><code>subset</code></td>
<td>
<p>An expression that evaluates to a logical or integer indexing vector. It is evaluated in 
<code>data</code>. Only the resulting rows of <code>data</code> are used for the plot.</p>
</td></tr>
<tr valign="top"><td><code>lattice.options</code></td>
<td>
<p>A list that could be supplied to <code><a href="../../lattice/html/lattice.options.html">lattice.options</a></code></p>
</td></tr>
<tr valign="top"><td><code>bwidth, dwidth</code></td>
<td>
<p>a numeric value for the confidence interval bar width and dodge width, respectively. 
In the latter case, a dodge is only used when multiple models are specified in the formula.</p>
</td></tr>
</table>


<h3>Details</h3>

<p><code>calibration.formula</code> is used to process the data and <code>xyplot.calibration</code> is used to create the plot.
</p>
<p>To construct the calibration plot, the following steps are used for each model:
</p>

<ol>
<li><p> The data are split into <code>cuts - 1</code> roughly equal groups by their class probabilities
</p>
</li>
<li><p> the number of samples with true results equal to <code>class</code> are determined
</p>
</li>
<li><p> the event rate is determined for each bin</p>
</li></ol>

<p><code>xyplot.calibration</code> produces a plot of the observed event rate by the mid-point of the bins.
</p>
<p>This implementation uses the <span class="pkg">lattice</span> function <code><a href="../../lattice/html/xyplot.html">xyplot</a></code>, so plot 
elements can be changed via panel functions, <code><a href="../../lattice/html/trellis.par.get.html">trellis.par.set</a></code> or 
other means. <code>calibration</code> uses the panel function <code><a href="calibration.html">panel.calibration</a></code> by default, but 
it can be changed by passing that argument into <code>xyplot.calibration</code>.
</p>
<p>The following elements are set by default in the plot but can be changed by passing new values into 
<code>xyplot.calibration</code>: <code>xlab = "Bin Midpoint"</code>, <code>ylab = "Observed Event Percentage"</code>, 
<code>type = "o"</code>, <code>ylim = extendrange(c(0, 100))</code>,<code>xlim = extendrange(c(0, 100))</code> and 
<code>panel = panel.calibration</code>
</p>
<p>For the <code>ggplot</code> method, confidence intervals on the estimated proportions (from 
<code><a href="../../stats/html/binom.test.html">binom.test</a></code>) are also shown.
</p>


<h3>Value</h3>

<p><code>calibration.formula</code> returns a list with elements:
</p>
<table summary="R valueblock">
<tr valign="top"><td><code>data</code></td>
<td>
<p>the data used for plotting</p>
</td></tr>
<tr valign="top"><td><code>cuts</code></td>
<td>
<p>the number of cuts</p>
</td></tr>
<tr valign="top"><td><code>class</code></td>
<td>
<p>the event class</p>
</td></tr>
<tr valign="top"><td><code>probNames</code></td>
<td>
<p>the names of the model probabilities</p>
</td></tr>
</table>
<p><code>xyplot.calibration</code> returns a <span class="pkg">lattice</span> object
</p>


<h3>Author(s)</h3>

<p>Max Kuhn, some <span class="pkg">lattice</span> code and documentation by Deepayan Sarkar
</p>


<h3>See Also</h3>

<p><code><a href="../../lattice/html/xyplot.html">xyplot</a></code>, <code><a href="../../lattice/html/trellis.par.get.html">trellis.par.set</a></code>
</p>


<h3>Examples</h3>

<pre>
## Not run: 
data(mdrr)
mdrrDescr &lt;- mdrrDescr[, -nearZeroVar(mdrrDescr)]
mdrrDescr &lt;- mdrrDescr[, -findCorrelation(cor(mdrrDescr), .5)]


inTrain &lt;- createDataPartition(mdrrClass)
trainX &lt;- mdrrDescr[inTrain[[1]], ]
trainY &lt;- mdrrClass[inTrain[[1]]]
testX &lt;- mdrrDescr[-inTrain[[1]], ]
testY &lt;- mdrrClass[-inTrain[[1]]]

library(MASS)

ldaFit &lt;- lda(trainX, trainY)
qdaFit &lt;- qda(trainX, trainY)

testProbs &lt;- data.frame(obs = testY,
                        lda = predict(ldaFit, testX)$posterior[,1],
                        qda = predict(qdaFit, testX)$posterior[,1])

calibration(obs ~ lda + qda, data = testProbs)

calPlotData &lt;- calibration(obs ~ lda + qda, data = testProbs)
calPlotData

xyplot(calPlotData, auto.key = list(columns = 2))

## End(Not run)

</pre>

<hr /><div style="text-align: center;">[Package <em>caret</em> version 6.0-82 <a href="00Index.html">Index</a>]</div>
</body></html>
